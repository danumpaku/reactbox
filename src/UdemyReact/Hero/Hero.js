import React from 'react';
import './Hero.css';

const Hero = (props) => 
    <div className = "Hero">
        <p onClick={props.click}> I'm a {props.name}, i'm {props.age} years old.</p>
        <p> {props.children} </p>
    </div>

export default Hero;