import React, { useState } from 'react';
import Hero from './Hero/Hero';
import RegisterHero from './RegisterHero/RegisterHero'

const UdemyReact = (props) => {

    const [currentState, setState] = useState({
        heroes: [
            {name: 'Deku', age: 16},
            {name: 'Ochako', age: 15},
            {name: 'Todoroki', age: 15},
            {name: 'Naota', age: 27}
        ],
        newHero: {realName: '', heroName: '', age: ''}
    });

    console.log(currentState);

    const switchNamesHandler = (idx, newName) => {
        var heroes = currentState.heroes;
        var newHero = currentState.newHero;
        heroes[idx].name = newName;
        setState({heroes, newHero});
    };

    const onChangeRealNameHandler = (event) => {
        var heroes = currentState.heroes;
        var newHero = currentState.newHero;
        newHero.realName = event.target.value;
        setState({heroes, newHero});
    };

    const onChangeHeroNameHandler = (event) => {
        var heroes = currentState.heroes;
        var newHero = currentState.newHero;
        newHero.heroName = event.target.value;
        setState({heroes, newHero});
    };

    const onChangeAgeHandler = (event) => {
        var heroes = currentState.heroes;
        var newHero = currentState.newHero;
        newHero.age = event.target.value;
        setState({heroes, newHero});
    };

    const buttonStyle = {
        backgroundColor: 'white',
        font: 'inherit',
        border: '1px solid blue',
        padding: '8px',
        cursor: 'pointer'
    }
 
    return (
        <div className='UdemyReact'>
            <h1>Udemy React</h1>
            <Hero name = {currentState.heroes[0].name} 
                    age = {currentState.heroes[0].age} 
                    click = {switchNamesHandler.bind(this, 0, 'Midoriya Izuku')} >
                    I'm the All might's biggest fan</Hero>
            <Hero name = {currentState.heroes[1].name} 
                    age = {currentState.heroes[1].age} 
                    click = {switchNamesHandler.bind(this, 1, 'Uraraka Ochako')} /> 
            <Hero name = {currentState.heroes[2].name} 
                    age = {currentState.heroes[2].age} 
                    click = {switchNamesHandler.bind(this, 2, 'Todoroki Shoto')} />
            <Hero name = {currentState.heroes[3].name} 
                    age = {currentState.heroes[3].age} 
                    click = {switchNamesHandler.bind(this, 3, 'Daniel Numpaque')} />
            
            <button style = {buttonStyle}
            onClick = {() => {  //Es más optimo usar bind que la funcion anónima
                                        //Por lo que se debe usar la funcion anonima
                                        //unicamente cuando sea necesario
                switchNamesHandler (0, 'Deku');
                switchNamesHandler (1, 'Uravity');
                switchNamesHandler (2, 'Shoto');
                switchNamesHandler (2, 'Naota');
            }} >Hide Real Names</button>

            <p>You are {currentState.newHero.realName} but you will be known as {currentState.newHero.heroName}. Your age is {currentState.newHero.age}</p>
            <RegisterHero   onChangeRealName = {onChangeRealNameHandler}
                            onChangeHeroName = {onChangeHeroNameHandler} 
                            onChangeAge = {onChangeAgeHandler} />
        </div>
    );
    
};

export default UdemyReact;