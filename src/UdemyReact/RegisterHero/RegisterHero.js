import React from 'react';

const RegisterHero = (props) => 
    <div className = "NewHero">
        <h4>New Hero</h4>
        <form>
        <div>
            <label>Real Name</label>
            <input type = "text" onChange = {props.onChangeRealName} />
        </div>
        <div>
            <label>Hero Name</label>
            <input type = "text" onChange = {props.onChangeHeroName} />
        </div>
        <div>
            <label>Age</label>
            <input type = "text" onChange = {props.onChangeAge} />
        </div>
        <button type = "submit">Registrar</button>
        </form>
        
    </div>

export default RegisterHero;
