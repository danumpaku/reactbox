import React from 'react';
import './App.css';
import UdemyReact from './UdemyReact/UdemyReact'
import MyLab from './MyLab/MyLab'

function App() {
  return (
    <div className="App">
      <h1>Hola Mundo</h1>
      <UdemyReact />
      <MyLab />
    </div>
  );
  // return React.createElement('div', {className: "App"}, React.createElement('h1', null, 'Hola Mundo'));
}

export default App;
